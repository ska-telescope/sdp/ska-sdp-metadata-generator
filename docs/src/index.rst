Welcome to ska-sdp-metadata-generator's documentation!
======================================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   api/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
