"""Wait for Docker before running tests."""

import asyncio
import logging
import signal
import sys
import threading
import time

import etcd3

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

ETCD_HOSTNAME = "localhost"
ETCD_PORT = 2379

TIMEOUT_SECS = 20.0
WAIT_ON_FAIL_SECS = 2.0


async def connect_etcd(event):
    """Wait for connection to etcd."""

    def task(event: threading.Event):
        start = time.time()
        while not event.is_set():
            try:
                logger.info("Attempting to connect to etcd host %s", ETCD_HOSTNAME)
                c = etcd3.Etcd3Client(host=ETCD_HOSTNAME, port=ETCD_PORT)
                c.get_response("/")
                logger.info(f"etcd at {ETCD_HOSTNAME} connected")
                return
            except asyncio.CancelledError:
                break
            except Exception as ex:
                logger.info("waiting for etcd host %s..", ETCD_HOSTNAME)
                time.sleep(WAIT_ON_FAIL_SECS)
                if time.time() - start >= TIMEOUT_SECS:
                    raise TimeoutError(f"Could not connect to {ETCD_HOSTNAME}:{ETCD_PORT}") from ex
        raise asyncio.CancelledError("Connection cancelled")

    await asyncio.to_thread(task, event)


async def await_services():
    """Wait for services."""
    exit_event = threading.Event()

    def shutdown():
        for task in asyncio.all_tasks():
            task.cancel()

    asyncio.get_running_loop().add_signal_handler(signal.SIGINT, shutdown)
    res = await asyncio.gather(
        connect_etcd(exit_event),
    )
    sys.exit(1 if any(res) else 0)


if __name__ == "__main__":
    asyncio.run(await_services())
