#!/usr/bin/env python

"""Tests for `ska_sdp_metadata_generator` package."""

import datetime
import logging
import os

import pytest
import ska_sdp_config
from casacore import tables

import ska_sdp_metadata_generator.measurementset as ms
from ska_sdp_metadata_generator import metadata_generator, utils

# pylint: disable=redefined-outer-name

LOG = logging.getLogger("metadata-generator-test")
LOG.setLevel(logging.DEBUG)

TODAY_DATE = datetime.datetime.now().strftime("%Y%m%d")
INPUT_TEST_FILE = "data/unknown_test_file.txt"
INPUT_MS = "data/sim-vis.ms"
EXPECTED_SUBTABLES = [
    "ANTENNA",
    "DATA_DESCRIPTION",
    "FEED",
    "FLAG_CMD",
    "FIELD",
    "HISTORY",
    "OBSERVATION",
    "POINTING",
    "POLARIZATION",
    "PROCESSOR",
    "SPECTRAL_WINDOW",
    "STATE",
]


def test_read_ms():
    """Read in the MS file for testing."""
    maintable = tables.table(INPUT_MS, readonly=True, ack=False)
    subtables = []
    for subtab in maintable.getsubtables():
        file_name = os.path.basename(subtab)
        subtables.append(file_name)

    assert subtables == EXPECTED_SUBTABLES


@pytest.mark.parametrize(
    "input_data, expected_result",
    [
        ([], ""),  # Test empty input
        (None, ""),  # Test None input
        ([5, 6, 7, 8], "RR/RL/LR/LL"),  # Test valid input
        ([5, 13, 8], "Unrecognized correlation type: 13"),  # Test invalid input
    ],
)
def test_stokes_polarisations(input_data, expected_result):
    """
    Test the stokes_polarisations function with various input scenarios.

    Parameters:
    input_data (list or None): The input data to be passed to the stokes_polarisations function.
    expected_result (str or ValueError): The expected result or error message.
    """
    if "Unrecognized" in expected_result:
        with pytest.raises(ValueError, match=expected_result):
            ms.stokes_polarisations(input_data)
    else:
        result = ms.stokes_polarisations(input_data)
        assert result == expected_result


time_to_mjd_test_cases = [
    (0.0, 0.0),  # Test with zero seconds
    (86400.0, 1.0),  # Test with one day in seconds
    (3600.0, 0.041666666666666664),  # Test with one hour in seconds
    (-3600.0, -0.041666666666666664),  # Test with negative one hour in seconds
]


@pytest.mark.parametrize("input_seconds, expected_mjd", time_to_mjd_test_cases)
def test_time_to_mjd(input_seconds, expected_mjd):
    """Test the time_to_mjd function with various input scenarios."""
    result = utils.time_to_mjd(input_seconds)

    # Check if the result matches the expected MJD, considering a small tolerance
    tolerance = 1e-6
    assert abs(result - expected_mjd) < tolerance


check_diameter_test_cases = [
    ([], ""),  # Test with an empty list
    ([10.0, 10.0, 10.0], 10.0),  # Test with all values the same
    ([5.0, 7.0, 9.0], "various"),  # Test with various values
    ([3.14, 3.14, 3.14, 3.14], 3.14),  # Test with all values the same (floating-point)
]


@pytest.mark.parametrize("diameters, expected_result", check_diameter_test_cases)
def test_check_diameter(diameters, expected_result):
    """Test the check_diameter function with various input scenarios."""
    result = ms.check_diameter(diameters)
    assert result == expected_result


@pytest.fixture(name="maintable")
def maintable_fixture():
    """Extract maintable from test MeasurementSet."""
    maintable = tables.table(INPUT_MS, readonly=True, ack=False)
    yield maintable
    maintable.close()


def test_existing_subtable(maintable):
    """Try to extract an existing subtable."""
    subtable = ms.subtable(maintable, "ANTENNA")
    assert subtable is not None
    subtable_name = os.path.basename(subtable.name())
    assert subtable_name == "ANTENNA"


def test_non_existing_subtable(maintable):
    """Try to extract a non-existing subtable."""
    subtable = ms.subtable(maintable, "SOURCE")
    assert subtable is None


def test_generate_metadata():
    """Test that the Obscore dictionary for the MS is written correctly."""
    # Define the expected Obscore output as a dictionary
    expected_obscore = {
        "obscore": {
            "access_estsize": 4636,
            "access_format": "application/unknown",
            "calib_level": 0,
            "dataproduct_type": "MS",
            "em_xel": 4,
            "facility_name": "ASKAP simulator",
            "instrument_name": "SKA1_Low",
            "o_ucd": "stat.fourier",
            "obs_collection": "Unknown",
            "obs_publisher_did": "",
            "pol_states": "XX/XY/YX/YY",
            "pol_xel": 4,
            "s_dec": -0.4667010419832837,
            "s_ra": 0.0,
            "t_exptime": 120.0,
            "t_max": 57196.92225554443,
            "t_min": 57196.92086665554,
            "t_resolution": 0.9,
            "target_name": "",
        }
    }
    expected_obscore_radio = {
        "obscore_radio": {
            "f_max": 150.05000000000004,
            "f_min": 149.9,
            "instrument_ant_diameter": 35.0,
            "instrument_ant_number": 4,
        }
    }

    # Run metadata_generator.py with the test MS file as input
    metadata = metadata_generator.generate_metadata_from_generator(INPUT_MS)

    raw_metadata = metadata.get_data()
    assert raw_metadata["obscore"] == expected_obscore["obscore"]
    assert raw_metadata["obscore_radio"] == expected_obscore_radio["obscore_radio"]
    assert isinstance(raw_metadata["execution_block"], str)


def test_generate_metadata_basic():
    """Verify that the basic metadata generator is writing metadata as expected."""
    expected_obscore = {
        "obscore": {
            "access_estsize": 0,
            "access_format": "application/unknown",
            "dataproduct_type": "Unknown",
            "facility_name": "SKA-Observatory",
            "instrument_name": "Unknown",
            "obs_collection": "Unknown",
        }
    }

    # Run function with the test text file as input
    metadata_object = metadata_generator.generate_metadata_from_generator(INPUT_TEST_FILE)

    generated_meta = metadata_object.get_data()
    assert isinstance(generated_meta["execution_block"], str)
    assert generated_meta["interface"] == "http://schema.skao.int/ska-data-product-meta/0.1"

    generated_obscore = generated_meta["obscore"]
    assert generated_obscore == expected_obscore["obscore"]
    # Check the set of keys in 'files' matches what is expected
    expected_file_keys = ["crc", "description", "path", "size", "status"]
    for file_dict in generated_meta["files"]:
        assert set(file_dict.keys()) == set(expected_file_keys)


POST_URL = "http://localhost:8000/ingestnewdataproduct"
POST_FULL_PATH_NAME = "test-directory/ska-data-product.yaml"


def test_post_url_on_write(requests_mock):
    """Test that the write hook will post metadata file info to a URL."""
    # mock a response for this URL, a copy of the normal response from ska-sdp-dataproduct-api
    requests_mock.post(
        POST_URL, text="New data product metadata file loaded and store index updated"
    )

    utils.post_url_on_write(POST_URL, POST_FULL_PATH_NAME)


@pytest.fixture(scope="module")
def prefix():
    """Database prefix (taken from ska-sdp-config/tests/test_execution_block.py)."""  # noqa: D402
    return "/__test_eb"


@pytest.fixture
def cfg():
    """Pytest fixture to create a ska_sdp_config.Config instance."""
    config = ska_sdp_config.Config()
    for txn in config.txn():
        for eb_id in txn.execution_block.list_keys():
            txn.execution_block(eb_id).delete(recurse=True)
    yield config
    for txn in config.txn():
        for eb_id in txn.execution_block.list_keys():
            txn.execution_block(eb_id).delete(recurse=True)


def test_create_eb_id(cfg):  # noqa: C901
    """Create and delete execution block IDs."""
    # Check execution block list is empty

    for txn in cfg.txn():
        # Check execution block list is empty
        eb_id_metas = txn.execution_block.list_keys()
        assert eb_id_metas == []

    # Create first execution block
    eb1_id_meta = utils.create_eb_id()

    for txn in cfg.txn():
        # Check execution block list has entry
        eb_id_metas = txn.execution_block.list_keys()
        assert eb_id_metas == [eb1_id_meta]

    # Create second execution block
    eb2_id_meta = utils.create_eb_id()

    for txn in cfg.txn():
        # Check execution block list has both entries
        eb_id_metas = txn.execution_block.list_keys()
        assert eb_id_metas == sorted([eb1_id_meta, eb2_id_meta])

        # Verify execution block IDs are unique
        assert eb1_id_meta != eb2_id_meta

    for txn in cfg.txn():
        # delete EB and its state
        txn.execution_block(eb1_id_meta).delete(recurse=True)
        txn.execution_block(eb2_id_meta).delete(recurse=True)

        # check that EB is deleted
        eblocks = txn.processing_block.list_keys(key_prefix=prefix)
        assert eb1_id_meta not in eblocks
        assert eb2_id_meta not in eblocks
        assert eb1_id_meta not in eblocks
        assert eb2_id_meta not in eblocks
