include .make/base.mk
include .make/python.mk
include .make/oci.mk

DOCS_SPHINXOPTS = -W --keep-going
PYTHON_LINE_LENGTH = 99

docs-pre-build:
	poetry config virtualenvs.create false
	poetry install --only main,docs

.PHONY: docs-pre-build

# GitlabCI services used in CI, docker compose for local testing only
ifndef GITLAB_CI
SERVICES_UP=test-services-up
SERVICES_DOWN=test-services-down
endif

python-pre-test: ${SERVICES_UP}
	tar xfz data/sim-vis.ms.tar.gz -C data
	python3 tests/scripts/wait_for_test_services.py

python-post-test: ${SERVICES_DOWN}

test-services-up:
	docker compose --file tests/test-services.docker-compose.yaml -p metagen-test-services up --detach

test-services-down:
	docker compose --file tests/test-services.docker-compose.yaml -p metagen-test-services down