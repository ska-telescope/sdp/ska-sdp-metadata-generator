"""This module is called when the dataproduct is an unidentified filetype."""

import logging

from benedict import benedict
from ska_sdp_dataproduct_metadata import MetaData

from .utils import build_files_list, get_file_or_dir_size


def generate_metadata_basic(dataproduct: str, eb_id: str | None) -> MetaData:
    """Generate minimal metadata for unknown file types."""
    meta = MetaData()
    data: benedict = meta.get_data()

    if eb_id is not None:
        data.execution_block = eb_id
    else:
        logging.error("EB_ID not provided")
        data.execution_block = ""

    data.obscore.access_estsize = int(get_file_or_dir_size(dataproduct))
    # TODO: discuss rounding for small files (< 1kb)
    # dataproduct-metadata enforces using int(), but small files will receive 0kb.

    data.files = build_files_list(dataproduct)

    return meta
