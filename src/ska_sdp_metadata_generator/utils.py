"""Utilities for the `ska_sdp_metadata_generator` module."""

import binascii
import json
import logging
import os

import requests
import ska_sdp_config
from astropy.time import Time, TimeDelta

logger = logging.getLogger(__name__)


def time_to_mjd(mjd_in_secs):
    """Convert from seconds to days, and take leap seconds into account."""
    return (
        Time(0.0, format="mjd", scale="tai") + TimeDelta(mjd_in_secs, format="sec", scale="tai")
    ).mjd


def get_file_or_dir_size(path="."):
    """Calculate file size in kB for a single file or directory."""
    if os.path.isfile(path) and not os.path.islink(path):  # Exclude symbolic links
        return os.path.getsize(path) / 1000

    if os.path.isdir(path):
        total_size = 0
        for dirpath, _, filenames in os.walk(path):
            for filename in filenames:
                file_path = os.path.join(dirpath, filename)
                if not os.path.islink(file_path):  # Exclude symbolic links
                    total_size += os.path.getsize(file_path)
        return total_size / 1000

    raise ValueError(f"Filesystem path {path} does not exist.")


def get_file_crc(file_path):
    """Calculate CRC32 for file."""
    with open(file_path, "rb") as file:
        buffer = file.read()
        buffer = binascii.crc32(buffer) & 0xFFFFFFFF
        return f"{buffer:08x}"


def calculate_directory_crc(directory_path):
    """Calculate the CRC32 for a directory and its contents."""
    crc = 0

    for root, _, files in os.walk(directory_path):
        for name in files:
            file_path = os.path.join(root, name)

            with open(file_path, "rb") as file:
                while True:
                    chunk = file.read(1024)
                    if not chunk:
                        break
                    crc = binascii.crc32(chunk, crc) & 0xFFFFFFFF

    return f"{crc:08x}"


def build_files_list(path="."):
    """Build a list of all files within a given directory or a single file."""
    files_list = []

    if os.path.isfile(path):
        file_path = os.path.abspath(path)
        size = os.path.getsize(file_path)
        crc = get_file_crc(file_path)

        files_list.append(
            {
                "crc": crc,
                "description": "",
                "path": os.path.basename(file_path),
                "size": size,
                "status": "done",  # file status: working, done or failure
            }
        )
    elif os.path.isdir(path):
        for root, _, files in os.walk(path, topdown=False):
            for name in files:
                file_path = os.path.join(root, name)
                relative_path = os.path.relpath(file_path, path)

                # determine size
                # NB: this is in bytes, whereas obscore.access_estsize is in KB
                size = os.path.getsize(file_path)

                # determine crc for file
                crc = get_file_crc(file_path)

                files_list.append(
                    {
                        "crc": crc,
                        "description": "",
                        "path": relative_path,
                        "size": size,
                        "status": "done",  # file status: working, done or failure
                    }
                )
    else:
        raise ValueError("Path must be a file or directory.")

    return files_list


def build_files_list_single_level(path="."):
    """Calculate the total size and CRC of a directory and its contents."""
    if os.path.isfile(path):
        # If the input path is a file, calculate size and CRC for the file
        size = os.path.getsize(path)
        crc = get_file_crc(path)
    elif os.path.isdir(path):
        # If the input path is a directory, recursively calculate size and CRC
        crc = calculate_directory_crc(path)
        size = 0
        for root, _, files in os.walk(path, topdown=False):
            for name in files:
                file_path = os.path.join(root, name)
                size += os.path.getsize(file_path)
    else:
        raise ValueError("invalid or non-existent paths")

    return [
        {
            "crc": crc,
            "description": "",
            "path": path,
            "total_size": size,  # Total size of the directory and its contents
            "status": "done",  # file status: working, done or failure
        }
    ]


def post_url_on_write(url: str, filepath: str):
    """HTTP POST details of the given file to the given url."""
    payload = json.dumps({"fileName": filepath, "relativePathName": filepath})
    headers = {"Content-Type": "application/json"}

    try:
        requests.request("POST", url, headers=headers, data=payload, timeout=2)
        logger.info("POSTed filepath (%s) to %s", filepath, url)
    except requests.Timeout:
        logger.exception("Timeout POSTing file path to: %s", url)
    except requests.ConnectionError:
        logger.exception("Connection Error POSTing file path to: %s", url)


def create_eb_id() -> str:
    """Create a new unique meta execution block ID.

    The created execution block ID with prefix `eb-meta` is additionally
    registered to a new empty entity in the SDP configuration database.

    Returns:
        str: execution block ID.
    """
    config = ska_sdp_config.Config()
    for txn in config.txn():
        eb_id_meta = txn.new_execution_block_id("meta")
        txn.execution_block(eb_id_meta).create({})

    return eb_id_meta
