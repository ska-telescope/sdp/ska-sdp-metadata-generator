"""Main module."""
import importlib
import logging
import pathlib
from typing import Callable

from ska_sdp_dataproduct_metadata import MetaData

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


FILETYPE_GENERATORS_BY_MODULE_NAME = {
    "ms": "ska_sdp_metadata_generator.measurementset",
}


def _find_generator_by_module(file_extension: str) -> Callable:
    """Identify the file type and retrieve the corresponding generator function."""
    module_name = FILETYPE_GENERATORS_BY_MODULE_NAME.get(file_extension)
    if module_name is None:
        module_name = "ska_sdp_metadata_generator.unknown_extension"
        module = importlib.import_module(module_name)
        generator_function = getattr(module, "generate_metadata_basic", None)
    else:
        module = importlib.import_module(module_name)
        generator_function = getattr(module, "generate_metadata", None)

    logger.info("module name: %s", module)
    return generator_function


def generate_metadata_from_generator(file: str, eb_id: str | None = None) -> MetaData:
    """Generate the metadata of the input file."""
    path = pathlib.Path(file)
    logger.info("writing metadata for dataproduct: %s", path)
    # TODO: we don't want to guess the file type
    generator = _find_generator_by_module(path.suffix.lower()[1:])
    meta = generator(str(path), eb_id)

    validation_errors = validate(meta.get_data())

    for validation_error in validation_errors:
        logger.error(
            "Data product schema validation: %s: %s",
            ".".join(validation_error.relative_path),
            str(validation_error.message),
        )

    if len(validation_errors) == 0:
        return meta  # Return the MetaData object

    # If validation errors occurred, raise an exception
    raise ValueError("Validation errors occurred, metadata generation failed.")


def validate(metadata: dict):
    """Validate a metadata dictionary against the schema."""
    errors = []

    validator_errors = MetaData.validator.iter_errors(metadata)

    # Loop over the errors
    for validator_error in validator_errors:
        errors.append(validator_error)

    return errors
