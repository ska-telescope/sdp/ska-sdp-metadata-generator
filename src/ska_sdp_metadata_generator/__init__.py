"""Top-level package for ska-sdp-metadata-generator."""

from .measurementset import check_diameter, stokes_polarisations
from .metadata_generator import generate_metadata_from_generator, validate
from .utils import time_to_mjd

__all__ = [
    "generate_metadata_from_generator",
    "validate",
    "stokes_polarisations",
    "time_to_mjd",
    "check_diameter",
]

__author__ = """Nadia Steyn"""
__email__ = "nadia.steyn@uwa.edu.au"
__version__ = "0.0.5"
