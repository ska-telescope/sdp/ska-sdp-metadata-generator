"""This module is called when the dataproduct is identified as a MeasurementSet."""

import logging

from benedict import benedict
from casacore import tables
from ska_sdp_dataproduct_metadata import MetaData, ObsCore

from .utils import build_files_list_single_level, get_file_or_dir_size, time_to_mjd


def stokes_polarisations(corr_types):
    """Return polarisations, separated by '/'."""
    stokes_types = {
        5: "RR",
        6: "RL",
        7: "LR",
        8: "LL",
        9: "XX",
        10: "XY",
        11: "YX",
        12: "YY",
    }

    if corr_types is None or not corr_types:
        return ""

    for value in corr_types:
        if value not in stokes_types:
            raise ValueError(f"Unrecognized correlation type: {value}")

    keys = [stokes_types[value] for value in corr_types]
    return "/".join(keys)


def check_diameter(diameters):
    """Check if all dish diameter values are the same."""
    if len(diameters) == 0:
        return ""
    if all(d == diameters[0] for d in diameters):
        return float(diameters[0])
    return "various"


def subtable(tbl: tables.table, name: str, readonly=True):
    """Extract subtable from a MeasurementSet."""
    try:
        return tables.table(tbl.getkeyword(name), readonly=readonly, ack=False)
    except RuntimeError as err:
        print("ERROR:", err)
        return None


def _get_antdict(maintable):
    anttable = subtable(maintable, "ANTENNA")
    if not anttable:
        return {}

    dish_diameters = []
    for row in range(anttable.nrows()):
        dish_diameter = anttable.getcell("DISH_DIAMETER", row)
        dish_diameters.append(dish_diameter)
    antdict = {
        "instrument_ant_diameter": check_diameter(dish_diameters),
        "instrument_ant_number": int(anttable.nrows()),
    }

    return antdict


def _get_pointdict(maintable):
    pointtable = subtable(maintable, "POINTING")
    if pointtable:
        if pointtable.nrows() == 0:
            print("WARNING: POINTING table has", pointtable.nrows(), "rows")
            pointdict = {}
        else:
            pointdict = {
                "s_ra": float(pointtable.getcell("TARGET", 0)[0][0]),
                "s_dec": float(pointtable.getcell("TARGET", 0)[0][1]),
                "target_name": pointtable.getcell("NAME", 0),
            }
    else:
        print("WARNING: Missing sub-table: POINTING")
        pointdict = {}
    return pointdict


def _get_poldict(maintable):
    poltable = subtable(maintable, "POLARIZATION")
    if poltable:
        poldict = {
            "pol_states": stokes_polarisations(list(poltable.getcell("CORR_TYPE", 0))),
            "pol_xel": int(poltable.getcell("NUM_CORR", 0)),
        }
    else:
        print("WARNING: Missing sub-table: POLARIZATION")
        poldict = {}
    return poldict


def _get_swdict(maintable):
    swtable = subtable(maintable, "SPECTRAL_WINDOW")
    if swtable:
        for row in range(swtable.nrows()):
            swdict = {
                "f_min": float(swtable.getcell("CHAN_FREQ", row)[0] / 1e6),
                "f_max": float(swtable.getcell("CHAN_FREQ", row)[-1] / 1e6),
                "em_xel": int(swtable.getcell("NUM_CHAN", row)),
            }
        if swtable.nrows() > 1:
            print("WARNING: table ", swtable, "has", swtable.nrows(), "rows")
    else:
        print("WARNING: Missing sub-table: SPECTRAL_WINDOW")
        swdict = {}
    return swdict


def _get_obsdict(maintable):
    obstable = subtable(maintable, "OBSERVATION")
    if obstable:
        obsdict = {
            "facility_name": str(obstable.getcell("OBSERVER", 0)),
            "obs_publisher_did": str(obstable.getcell("PROJECT", 0)),  # duplicate?
            "instrument_name": str(obstable.getcell("TELESCOPE_NAME", 0)),
            "t_min": float(time_to_mjd(obstable.getcell("TIME_RANGE", 0)[0])),
            "t_max": float(time_to_mjd(obstable.getcell("TIME_RANGE", 0)[-1])),
            "t_exptime": float(obstable.getcell("TIME_RANGE", 0)[-1])
            - float(obstable.getcell("TIME_RANGE", 0)[0]),
        }
    else:
        print("WARNING: Missing sub-table: OBSERVATION")
        obsdict = {}
    return obsdict


def _retrieve_value(dictionary: dict, key: str, default=None):
    """If a value doesn't exist, the script shouldn't crash."""
    if key not in dictionary:
        print(f"WARNING: could not find value '{key}'")
    return dictionary.get(key, default)


def generate_metadata(ms_file: str, eb_id: str | None) -> MetaData:
    """Create metadata object from MeasurementSet dictionaries."""
    # TODO: handle compressed measurementsets
    # if is_compressed:
    #     uncompress_only_what_you_need_if_you_can(ms_file)
    #     and remove at the end
    maintable = tables.table(ms_file, readonly=True, ack=False)

    maintabdict = {
        "t_resolution": maintable.getcell("INTERVAL", 0),
    }

    antdict = _get_antdict(maintable)
    obsdict = _get_obsdict(maintable)
    swdict = _get_swdict(maintable)
    poldict = _get_poldict(maintable)
    pointdict = _get_pointdict(maintable)

    # Combine dictionaries:
    joined_dict = maintabdict | antdict | obsdict | poldict | pointdict | swdict

    meta = MetaData()
    data: benedict = meta.get_data()

    # TODO(YAN-XXXX): Extract EB_ID from measurement set
    if eb_id is not None:
        logging.warning("Provided EB_ID (%s) not checked against value inside MS", eb_id)
        data.execution_block = eb_id
    else:
        logging.error("EB_ID extraction from MS not implemented, using placeholder instead")
        data.execution_block = ""

    data.files = build_files_list_single_level(ms_file)

    data.obscore.access_estsize = int(get_file_or_dir_size(ms_file))
    data.obscore.access_format = ObsCore.AccessFormat.UNKNOWN.value
    data.obscore.calib_level = ObsCore.CalibrationLevel.LEVEL_0.value
    data.obscore.dataproduct_type = ObsCore.DataProductType.MS.value
    data.obscore.facility_name = ObsCore.SKA
    data.obscore.instrument_name = ObsCore.UNKNOWN
    data.obscore.o_ucd = ObsCore.UCD.FOURIER.value
    data.obscore.obs_collection = ObsCore.ObservationCollection.UNKNOWN.value
    data.obscore.em_xel = _retrieve_value(joined_dict, "em_xel")
    data.obscore.facility_name = _retrieve_value(joined_dict, "facility_name")
    data.obscore.instrument_name = _retrieve_value(joined_dict, "instrument_name")
    data.obscore.obs_publisher_did = _retrieve_value(joined_dict, "obs_publisher_did")
    data.obscore.pol_states = _retrieve_value(joined_dict, "pol_states")
    data.obscore.pol_xel = _retrieve_value(joined_dict, "pol_xel")
    data.obscore.s_dec = _retrieve_value(joined_dict, "s_dec", -1)
    data.obscore.s_ra = _retrieve_value(joined_dict, "s_ra", -1)
    data.obscore.t_exptime = _retrieve_value(joined_dict, "t_exptime")
    data.obscore.t_max = _retrieve_value(joined_dict, "t_max")
    data.obscore.t_min = _retrieve_value(joined_dict, "t_min")
    data.obscore.t_resolution = _retrieve_value(joined_dict, "t_resolution")
    data.obscore.target_name = _retrieve_value(joined_dict, "target_name", "")

    data.obscore_radio = {}
    data.obscore_radio.instrument_ant_number = _retrieve_value(
        joined_dict, "instrument_ant_number"
    )
    data.obscore_radio.instrument_ant_diameter = _retrieve_value(
        joined_dict, "instrument_ant_diameter"
    )
    data.obscore_radio.f_min = _retrieve_value(joined_dict, "f_min")
    data.obscore_radio.f_max = _retrieve_value(joined_dict, "f_max")

    # TODO: the following attributes need to be revised:
    # data.config.image = "artefact.skao.int/ska-sdp-script-vis-receive"
    # data.config.processing_block = "pb-test-20200425-00000"
    # data.config.processing_script = "vis-receive"
    # data.config.version = "0.6.0"
    # data.interface = "http://schema.skao.int/ska-data-product-meta/0.1"
    # data.obscore.obs_id = "pb-test-20200425-00000"
    # data.obscore.access_url = "console.cloud.google.com/storage/browser/ska1-simulation-data/"

    return meta
