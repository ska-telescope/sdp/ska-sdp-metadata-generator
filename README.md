# ska-sdp-metadata-generator

## Description
Utility to generate SKA data product metadata (YAML format) from data products such as Measurement Sets.

## Installation
1. Clone the repo
2. Run `poetry install`
3. Run `poetry shell`

## Usage
* The metadata-generator is called directly by the Data Lifecycle Management (DLM) ingest manager (via function-call).
* The metadata-generator supports different file types as 'plug-ins.'
* Right now it only has plug-ins for two file formats: Measurement Sets, and 'unknown_extension.'
* 'Unknown_extension' is a basic fallback plug-in to produce minimal metadata in the case where the file format of the input is not recognised.
* The metadata-generator returns the contents of the generated metadata in the form of a MetaData object. See [ska-sdp-dataproduct-metadata](https://developer.skao.int/projects/ska-sdp-dataproduct-metadata/en/latest/)
* The MetaData object is returned to the calling method.
* Implementing: The metadata-generator can send the MetaData object to the ska-sdp-dataproduct-api via a HTTP POST request.

## License
Copyright 2020 SKA Observatory
